test:
	docker-compose exec web pytest tests

sh:
	docker-compose exec web /bin/sh

update:
	docker cp requirements.txt "wha-api_web_1:/code/requirements.txt"
	docker-compose exec web pip install -r requirements.txt


