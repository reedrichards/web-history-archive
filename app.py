import redis
from flask import Flask, request, jsonify
from utils.transform import bulk_transform
from utils.load import load
from utils.publish import bulk_publish, initial_sync

# TODO write this
# from .utils import transform, load


app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


@app.route('/transform-and-load', methods=['POST'])
def transform_and_load():
    if request.method == 'POST':
        # validate: The request keys must be an array of dicts containing urls
        # and titles
        # [
        #     {
        #         "url": "https://www.tensorflow.org/deploy/distributed",
        #         "title": "Distributed TensorFlow  |  TensorFlow",
        #     }
        # ]
        # regex validation for url
        # string validation for title

        web_history = request.json
        transformed_history = bulk_transform(web_history)
        load(transformed_history)
        bulk_publish(transformed_history)
        resp = jsonify(success=True)
        return resp, 200


if __name__ == "__main__":
    # flask starts before mongo and elasticsearch, so lets wait a bit first
    import time
    time.sleep(30)
    initial_sync()
    app.run(host="0.0.0.0", debug=True)
