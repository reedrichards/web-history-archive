// alert(document.title);
// alert(window.location.toString());
var url = "https://thinkpad:8082/transform-and-load";
var data = [
  {
    url: window.location.toString(),
    title: document.title
  }
];

fetch(url, {
  method: "POST", // or 'PUT'
  body: JSON.stringify(data), // data can be `string` or {object}!
  headers: {
    "Content-Type": "application/json"
  }
})
  .then(res => res.json())
  .then(response => console.log("Success:", JSON.stringify(response)))
  .catch(error => console.error("Error:", error));
