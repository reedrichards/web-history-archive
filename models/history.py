from mongoengine import Document, URLField, StringField


class WebPage(Document):
    url = URLField()
    title = StringField(max_length=255)
    words = StringField()
    html = StringField()
    meta = {
        'indexes': [
            {'fields': ('url',), 'unique': True}
        ]
    }
