from mongoengine import connect
from mongoengine.errors import NotUniqueError
from models.history import WebPage


def load(transformed_history):
    """
    Takes a list of transformed history and loads it into mongodb
    """

    connect(
        'web-archive',
        username='root',
        password='example',
        authentication_source='admin',
        host='mongo',
        port=27017
    )

    # define our webpage Document

    pages = []
    for history in transformed_history:
        pages.append(WebPage(
            history["url"],
            history["title"],
            history["words"],
            history["html"]
        ))

    # add data to our database
    try:
        # Try to do a bulk update
        WebPage.objects.insert(pages)
    except NotUniqueError:
        # if they are are all not unique, itereate through
        # TODO low hanging fruit for optimization
        for page in pages:
            try:
                page.save()
            except NotUniqueError:
                pass
