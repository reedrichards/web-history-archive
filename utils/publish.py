from datetime import datetime
from elasticsearch import Elasticsearch
from mongoengine import connect

from models.history import WebPage


def publish(transformed_history):
    """
    publish a transformed history to elasticsearch
    """
    es = Elasticsearch(hosts=[{"host": 'elasticsearch'}])

    doc = {
        'url': transformed_history["url"],
        'words': transformed_history["words"],
        'title': transformed_history["title"],
        'html': transformed_history["html"],
        'timestamp': datetime.now(),
    }
    res = es.index(index="web-history", doc_type='history',  body=doc)
    return res['result']


def bulk_publish(transformed_history_list):
    return [publish(history) for history in transformed_history_list]


def initial_sync():
    """
    Publish all histories in database to elasticsearch 
    """
    connect(
        'web-archive',
        username='root',
        password='example',
        authentication_source='admin',
        host='mongo',
        port=27017
    )

    bulk_publish(WebPage.objects)
