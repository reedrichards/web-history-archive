import requests
import bs4


def transform(history):
    """
    transforms a history
    history = {
        "url": "https://www.tensorflow.org/deploy/distributed",
        "title": "Distributed TensorFlow  TensorFlow",
     }
    into a transformed history with words and html

    """
    print(history)
    res = requests.get(history["url"])

    try:
        res.raise_for_status()
    except Exception as exc:
        # eventually do something here
        print('There was a problem: %s' % (exc))

    # load into web parser

    soup = bs4.BeautifulSoup(res.content)

    # add words and html to history

    history["words"] = soup.get_text()
    history["html"] = soup.prettify()
    return history


def bulk_transform(history_list):
    return [transform(history) for history in history_list]
